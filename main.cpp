#include <iostream>
#include <string>
#include <cstring>
#include <chrono>
#include "ec_system.hpp"

using namespace ec_system;

#define COMPONENT_LEAK_CHECK_ignore_member_resources(ClassName) \
    ClassName() \
    {   \
        leak_check = (char*)malloc(7*sizeof(char)); \
    }   \
    ClassName(const ClassName& other)   \
    {   \
        leak_check = (char*)malloc(7*sizeof(char)); \
        memcpy(leak_check, other.leak_check, sizeof(char)*7);   \
    }   \
    ~ClassName()    \
    {   \
        free(leak_check);   \
    }   \
    ClassName& operator=(const ClassName& other)    \
    {   \
        leak_check = (char*)realloc(leak_check, 7*sizeof(char)); \
        memcpy(leak_check, other.leak_check, sizeof(char)*7);   \
        return *this;   \
    }   \
    char* leak_check;

struct Component_A
{
    Component_A(){}
    Component_A(std::string s)
    {
        i = s.size();
    }
    int i;
};

struct Component_B
{
    COMPONENT_LEAK_CHECK_ignore_member_resources(Component_B)

    float f;
};

struct Component_C
{
    COMPONENT_LEAK_CHECK_ignore_member_resources(Component_C)

    char c[10000];
};


struct Component_S
{
  Component_S(int k)
  {
    k += 1;
    k -= 1;
    l=k;
  }
  int l;
};

struct Component_D{uint64_t u;}; struct Component_E{uint64_t u;}; struct Component_F{uint64_t u;}; struct Component_G{uint64_t u;};
struct Component_H{uint64_t u;}; struct Component_I{uint64_t u;}; struct Component_J{uint64_t u;}; struct Component_K{uint64_t u;};

bool test()
{
    const int number_entities_list[] = {0, 1, 2, 4, 10, 25, 60, 150, 400, 1'000, 2'000, 5'000, 10'000, 50'000, 100'000};

    for(auto number_entities : number_entities_list)
    {
        EntityManager em = EntityManager();
        for(int i = 0; i<number_entities; i++)
        {
            em.createEntity();
        }
				//std::cout << "HI1" << std::endl;

        for(auto entity : em.iterator<void>())
        {
            if((int)entity.getId() % 7 == 0)
            {
                em.removeEntity(entity);
                if(!entity.getId() % 4 == 0)
                {
                    em.createEntity();
                }
            }
        }
				//std::cout << "HI2" << std::endl;

        for(auto entity : em.iterator<void>())
        {
						//std::cout << "HI2.1" << std::endl;
            if((int)entity.getId() % 1 == 0)
            {
								//std::cout << "HI2.1.1" << std::endl;
                em.createComponent<Component_S>(entity, 5);
								//std::cout << "HI2.1.2" << std::endl;
                em.createComponent<Component_A>(entity, "Hello");
                em.get<Component_A>(entity).i = entity.getId();
                assert(em.get<Component_A>(entity).i == (int)entity.getId());

                if((int)entity.getId() % 3 == 1)
                {
                  em.removeComponent<Component_A>(entity);
                  em.createComponent<Component_A>(entity, "Hello");
                  em.get<Component_A>(entity).i = (int)entity.getId();
                  assert(em.get<Component_A>(entity).i == (int)entity.getId());
                }

                if((int)entity.getId() % 5 == 2)
                {
                  assert(em.has<Component_A>(entity));
                  em.get<Component_A>(entity).i = (int)entity.getId();
                  assert(em.get<Component_A>(entity).i == (int)entity.getId());
                }
			//std::cout << "HI2.1.2.3" << std::endl;
            }//std::cout << "HI2.2" << std::endl;
            if((int)entity.getId() % 2 == 0)
            {
                em.createComponent<Component_B>(entity);
                em.get<Component_B>(entity).f = ((float)entity.getId())/number_entities;
            }//std::cout << "HI2.3" << std::endl;
            if((int)entity.getId() % 5 == 0)
            {
                em.createComponent<Component_C>(entity);
                em.get<Component_C>(entity).c[0] = (int)entity.getId();
            }//std::cout << "HI2.4" << std::endl;
        }
				//std::cout << "HI3" << std::endl;

        for(auto entity : em.iterator<Component_C>())
        {
            if((int)entity.getId() % 3 == 0)
            {
                em.removeComponent<Component_C>(entity);
                if(!(int)entity.getId() % 4 == 0)
                {
                    em.createComponent<Component_C>(entity);
                    em.get<Component_C>(entity).c[0] = (int)entity.getId();
                }
            }
        }
				//std::cout << "HI4" << std::endl;

        for(auto entity : em.iterator<Component_A>())
        {
					if(em.get<Component_A>(entity).i != (int)entity.getId())
					{
						std::cout << 	"i: " << em.get<Component_A>(entity).i << ", id: " << (int)entity.getId() << std::endl;
					}
            assert(em.get<Component_A>(entity).i == (int)entity.getId());
        }
				//std::cout << "HI5" << std::endl;


        for(auto entity : em.iterator<Component_A>())
        {
            if((int)entity.getId() % ((number_entities/5)+1)  == 0)
            {
                for(auto entity2 : ++em.iterator<Component_A>(entity))
                {
                    assert(entity != entity2);
                    em.get<Component_A>(entity2).i += 1;
                }
            }
        }
				//std::cout << "HI6" << std::endl;

        for(auto entity : em.iterator<Component_A>())
        {
            int addition = 0;
            for(int i = (int)(*(em.iterator<Component_A>().begin())).getId(); i<(int)entity.getId(); i++)
            {
                if(i % ((number_entities/5)+1)  == 0)
                {
                    addition += 1;
                }
            }
            assert((int)entity.getId() + addition == em.get<Component_A>(entity).i);
        }


        em.each<Component_A, Component_B, Component_S>().run([](Component_A& a, Component_B& b, Component_S& s)
        {
          s.l = (a.i+1) * (19+a.i) * (b.f<0.999?1:2);
        });
        for(auto entity : em.iterator<Component_A, Component_B, Component_S>())
        {
          assert(
            em.get<Component_S>(entity).l ==
            (em.get<Component_A>(entity).i+1) * (19+em.get<Component_A>(entity).i) * (em.get<Component_B>(entity).f<0.999?1:2)
          );
        }
        [](const EntityManager& em)
        {
          em.each<Component_A, Component_B, Component_S>().run([](const Component_A& a, const Component_B& b, const Component_S& s)
          {
            assert(s.l == (a.i+1) * (19+a.i) * (b.f<0.999?1:2));
          });
        }(em);
    }
    return true;
}

void benchmark()
{
    if(true)
    {
        EntityManager em = EntityManager();
        {
            std::cout << "Create 1'000'000 entities: \t";

            std::chrono::high_resolution_clock::time_point startTime = std::chrono::high_resolution_clock::now();
            for(int i = 0; i<1'000'000; i++)
            {
                em.createEntity();
            }

            std::chrono::high_resolution_clock::time_point endTime = std::chrono::high_resolution_clock::now();
            std::cout << ((float)std::chrono::duration_cast<std::chrono::nanoseconds>(endTime - startTime).count()) / 1'000'000'000.0 << " s" << std::endl;
        }
        {
            std::cout << "Destroy 1'000'000 entities: \t";

            std::chrono::high_resolution_clock::time_point startTime = std::chrono::high_resolution_clock::now();

            for(auto entity : em.iterator<void>())
            {
                em.removeEntity(entity);
            }

            std::chrono::high_resolution_clock::time_point endTime = std::chrono::high_resolution_clock::now();
            std::cout << ((float)std::chrono::duration_cast<std::chrono::nanoseconds>(endTime - startTime).count()) / 1'000'000'000.0 << " s" << std::endl;
        }

    }
	const int rounds = 1;
	std::cout << "Rounds: " << rounds << std::endl;

    if(true)
    {
        EntityManager em = EntityManager();
        {
            std::cout << "Iterating over 1'000'000 entities, one component: \t";

            for(int i = 0; i<1'000'000; i++)
            {
                em.createComponent<Component_A>(em.createEntity());
            }

            std::chrono::high_resolution_clock::time_point startTime = std::chrono::high_resolution_clock::now();
						for(int o=0;o<rounds;++o)
            for(auto entity : em.iterator<Component_A>())
            {
                em.get<Component_A>(entity).i = 50;
            }

            std::chrono::high_resolution_clock::time_point endTime = std::chrono::high_resolution_clock::now();
            std::cout << ((float)std::chrono::duration_cast<std::chrono::nanoseconds>(endTime - startTime).count()) / 1'000'000'000.0 << " s" << std::endl;
        }
    }

    if(true)
    {
        EntityManager em = EntityManager();
        {
            std::cout << "Iterating over 1'000'000 entities, two components: \t";

            for(int i = 0; i<1'000'000; i++)
            {
                em.createComponent<Component_A>(em.createEntity());
                em.createComponent<Component_B>(em.createEntity());
            }
            std::chrono::high_resolution_clock::time_point startTime = std::chrono::high_resolution_clock::now();

						for(int o=0;o<rounds;++o)
            for(auto entity : em.iterator<Component_A, Component_B>())
            {
                em.get<Component_A>(entity).i = 50;
                em.get<Component_B>(entity).f = 0.5;
            }

            std::chrono::high_resolution_clock::time_point endTime = std::chrono::high_resolution_clock::now();
            std::cout << ((float)std::chrono::duration_cast<std::chrono::nanoseconds>(endTime - startTime).count()) / 1'000'000'000.0 << " s" << std::endl;
        }
    }

		if(true)
    {
        EntityManager em = EntityManager();
        {
            std::cout << "Iterating over 1'000'000 entities, two components, const: \t";

            for(int i = 0; i<1'000'000; i++)
            {
                em.createComponent<Component_A>(em.createEntity());
                em.createComponent<Component_B>(em.createEntity());
            }
				}
				[](const EntityManager& em)
				{
            std::chrono::high_resolution_clock::time_point startTime = std::chrono::high_resolution_clock::now();
						float counter = 0;
						for(int o=0;o<rounds;++o)
            for(auto entity : em.iterator<Component_A, Component_B>())
            {
                counter += em.get<Component_A>(entity).i + em.get<Component_B>(entity).f;
            }

            std::chrono::high_resolution_clock::time_point endTime = std::chrono::high_resolution_clock::now();
            std::cout << ((float)std::chrono::duration_cast<std::chrono::nanoseconds>(endTime - startTime).count()) / 1'000'000'000.0 << " s" << std::endl;
        }(em);
    }

    if(true)
    {
        EntityManager em = EntityManager();
        {
            std::cout << "Iterating over 1'000'000 entities, two components, half of the entities have all the components: \t";

            for(int i = 0; i<1'000'000; i++)
            {
                em.createComponent<Component_A>(em.createEntity());
                if(i%2 == 0)
                {
                    em.createComponent<Component_B>(em.createEntity());
                }
            }

            std::chrono::high_resolution_clock::time_point startTime = std::chrono::high_resolution_clock::now();
						for(int o=0;o<rounds;++o)
            for(auto entity : em.iterator<Component_A, Component_B>())
            {
                em.get<Component_A>(entity).i = 50;
                em.get<Component_B>(entity).f = 0.5;
            }

            std::chrono::high_resolution_clock::time_point endTime = std::chrono::high_resolution_clock::now();
            std::cout << ((float)std::chrono::duration_cast<std::chrono::nanoseconds>(endTime - startTime).count()) / 1'000'000'000.0 << " s" << std::endl;
        }
    }

    if(true)
    {
        EntityManager em = EntityManager();
        {
            std::cout << "Iterating over 1'000'000 entities, two components, only one entity has all the components: \t";

            for(int i = 0; i<1'000'000; i++)
            {
                em.createComponent<Component_A>(em.createEntity());
                if(i == 500'000)
                {
                    em.createComponent<Component_B>(em.createEntity());
                }
            }

            std::chrono::high_resolution_clock::time_point startTime = std::chrono::high_resolution_clock::now();
						for(int o=0;o<rounds;++o)
            for(auto entity : em.iterator<Component_A, Component_B>())
            {
                em.get<Component_A>(entity).i = 50;
                em.get<Component_B>(entity).f = 0.5;
            }

            std::chrono::high_resolution_clock::time_point endTime = std::chrono::high_resolution_clock::now();
            std::cout << ((float)std::chrono::duration_cast<std::chrono::nanoseconds>(endTime - startTime).count()) / 1'000'000'000.0 << " s" << std::endl;
        }
    }

    if(true)
    {
        EntityManager em = EntityManager();
        {
            std::cout << "Iterating over 1'000'000 entities, five components: \t";

            for(int i = 0; i<1'000'000; i++)
            {
                em.createComponent<Component_A>(em.createEntity());
                em.createComponent<Component_B>(em.createEntity());
                em.createComponent<Component_D>(em.createEntity());
                em.createComponent<Component_E>(em.createEntity());
                em.createComponent<Component_F>(em.createEntity());
            }

            std::chrono::high_resolution_clock::time_point startTime = std::chrono::high_resolution_clock::now();
						for(int o=0;o<rounds;++o)
            for(auto entity : em.iterator<Component_A, Component_B, Component_D, Component_E, Component_F>())
            {
                em.get<Component_A>(entity).i = 50;
                em.get<Component_B>(entity).f = 0.5;
                em.get<Component_D>(entity).u = 5;
                em.get<Component_E>(entity).u = 5;
                em.get<Component_F>(entity).u = 5;
            }

            std::chrono::high_resolution_clock::time_point endTime = std::chrono::high_resolution_clock::now();
            std::cout << ((float)std::chrono::duration_cast<std::chrono::nanoseconds>(endTime - startTime).count()) / 1'000'000'000.0 << " s" << std::endl;
        }
    }

    if(true)
    {
        EntityManager em = EntityManager();
        {
            std::cout << "Iterating over 1'000'000 entities, ten components: \t";

            for(int i = 0; i<1'000'000; i++)
            {
                em.createComponent<Component_A>(em.createEntity());
                em.createComponent<Component_B>(em.createEntity());
                em.createComponent<Component_D>(em.createEntity());
                em.createComponent<Component_E>(em.createEntity());
                em.createComponent<Component_F>(em.createEntity());
                em.createComponent<Component_G>(em.createEntity());
                em.createComponent<Component_H>(em.createEntity());
                em.createComponent<Component_I>(em.createEntity());
                em.createComponent<Component_J>(em.createEntity());
                em.createComponent<Component_K>(em.createEntity());
            }

            std::chrono::high_resolution_clock::time_point startTime = std::chrono::high_resolution_clock::now();
						for(int o=0;o<rounds;++o)
            for(auto entity : em.iterator<Component_A, Component_B, Component_D, Component_E, Component_F, Component_G, Component_H, Component_I, Component_J, Component_K>())
            {
                em.get<Component_A>(entity).i = 50;
                em.get<Component_B>(entity).f = 0.5;
                em.get<Component_D>(entity).u = 5;
                em.get<Component_E>(entity).u = 5;
                em.get<Component_F>(entity).u = 5;
                em.get<Component_G>(entity).u = 5;
                em.get<Component_H>(entity).u = 5;
                em.get<Component_I>(entity).u = 5;
                em.get<Component_J>(entity).u = 5;
                em.get<Component_K>(entity).u = 5;
            }

            std::chrono::high_resolution_clock::time_point endTime = std::chrono::high_resolution_clock::now();
            std::cout << ((float)std::chrono::duration_cast<std::chrono::nanoseconds>(endTime - startTime).count()) / 1'000'000'000.0 << " s" << std::endl;
        }
    }

    if(true)
    {
        EntityManager em = EntityManager();
        {
            std::cout << "Iterating over 1'000'000 entities, ten components, half of the entities have all the components: \t";

            for(int i = 0; i<1'000'000; i++)
            {
                em.createComponent<Component_A>(em.createEntity());
                em.createComponent<Component_B>(em.createEntity());
                em.createComponent<Component_D>(em.createEntity());
                em.createComponent<Component_E>(em.createEntity());
                em.createComponent<Component_F>(em.createEntity());
                em.createComponent<Component_G>(em.createEntity());
                em.createComponent<Component_H>(em.createEntity());
                em.createComponent<Component_I>(em.createEntity());
                em.createComponent<Component_J>(em.createEntity());
                if(i%2 == 0)
                {
                    em.createComponent<Component_K>(em.createEntity());
                }
            }

            std::chrono::high_resolution_clock::time_point startTime = std::chrono::high_resolution_clock::now();
						for(int o=0;o<rounds;++o)
            for(auto entity : em.iterator<Component_A, Component_B, Component_D, Component_E, Component_F, Component_G, Component_H, Component_I, Component_J, Component_K>())
            {
                em.get<Component_A>(entity).i = 50;
                em.get<Component_B>(entity).f = 0.5;
                em.get<Component_D>(entity).u = 5;
                em.get<Component_E>(entity).u = 5;
                em.get<Component_F>(entity).u = 5;
                em.get<Component_G>(entity).u = 5;
                em.get<Component_H>(entity).u = 5;
                em.get<Component_I>(entity).u = 5;
                em.get<Component_J>(entity).u = 5;
                em.get<Component_K>(entity).u = 5;
            }

            std::chrono::high_resolution_clock::time_point endTime = std::chrono::high_resolution_clock::now();
            std::cout << ((float)std::chrono::duration_cast<std::chrono::nanoseconds>(endTime - startTime).count()) / 1'000'000'000.0 << " s" << std::endl;
        }
    }

    if(true)
    {
        EntityManager em = EntityManager();
        {
            std::cout << "Iterating over 1'000'000 entities, ten components, only one entity has all the components: \t";

            for(int i = 0; i<1'000'000; i++)
            {
                em.createComponent<Component_A>(em.createEntity());
                em.createComponent<Component_B>(em.createEntity());
                em.createComponent<Component_D>(em.createEntity());
                em.createComponent<Component_E>(em.createEntity());
                em.createComponent<Component_F>(em.createEntity());
                em.createComponent<Component_G>(em.createEntity());
                em.createComponent<Component_H>(em.createEntity());
                em.createComponent<Component_I>(em.createEntity());
                em.createComponent<Component_J>(em.createEntity());
                if(i == 500'000)
                {
                    em.createComponent<Component_K>(em.createEntity());
                }
            }

            std::chrono::high_resolution_clock::time_point startTime = std::chrono::high_resolution_clock::now();
						for(int o=0;o<rounds;++o)
            for(auto entity : em.iterator<Component_A, Component_B, Component_D, Component_E, Component_F, Component_G, Component_H, Component_I, Component_J, Component_K>())
            {
                em.get<Component_A>(entity).i = 50;
                em.get<Component_B>(entity).f = 0.5;
                em.get<Component_D>(entity).u = 5;
                em.get<Component_E>(entity).u = 5;
                em.get<Component_F>(entity).u = 5;
                em.get<Component_G>(entity).u = 5;
                em.get<Component_H>(entity).u = 5;
                em.get<Component_I>(entity).u = 5;
                em.get<Component_J>(entity).u = 5;
                em.get<Component_K>(entity).u = 5;
            }

            std::chrono::high_resolution_clock::time_point endTime = std::chrono::high_resolution_clock::now();
            std::cout << ((float)std::chrono::duration_cast<std::chrono::nanoseconds>(endTime - startTime).count()) / 1'000'000'000.0 << " s" << std::endl;
        }
    }
}

struct Position
{
    float x,y;
};

struct Mass
{
    float m;
};

struct Timebomb
{
    Timebomb()
    {
        std::cout << "Activated timebomb ..." << std::endl;
    }
};

void example()
{
    EntityManager em = EntityManager();
    auto entity_0 = em.createEntity();
    auto entity_1 = em.createEntity();
    auto entity_2 = em.createEntity();
    auto entity_3 = em.createEntity();

    em.createComponent<Position>(entity_0, Position{10.0, 20.0});
    em.createComponent<Position>(entity_1, Position{5.5,  90.5});
    em.createComponent<Position>(entity_2, Position{30.0, 15.0});
    em.createComponent<Position>(entity_3, Position{8.0,  18.0});

    em.createComponent<Mass>(entity_0, Mass{40'000'000.0});
    em.createComponent<Mass>(entity_1, Mass{2'000'000.0});
    em.createComponent<Mass>(entity_3, Mass{800'000.0});

    em.createComponent<Timebomb>(entity_0);

    em.each<Position, Mass>().run([](Position& p, Mass& m){
      if(p.x + p.y < 27.5) //this is true only for entity_3
      {
        m.m *= 1000;
      }
    });

    for(auto entity_A : em.iterator<Position, Mass>())
    {
        for(auto entity_B : ++em.iterator<Position, Mass>(entity_A))
        {
            float dx = em.get<Position>(entity_B).x - em.get<Position>(entity_A).x;
            float dy = em.get<Position>(entity_B).y - em.get<Position>(entity_A).y;
            float distance_squared = dx*dx + dy*dy;
            if(distance_squared<0)
            {
                distance_squared*=-1;
            }
            float F = 6.67259e-11 * ((em.get<Mass>(entity_B).m * em.get<Mass>(entity_A).m) / (distance_squared));
            std::cout<<"Force between Entity " << entity_A.getId() << " and " << entity_B.getId() << ": "<< F << std::endl;
        }
    }

    for(auto entity : em.iterator<void>())
    {
        if(em.has<Timebomb>(entity))
        {
            em.removeEntity(entity);
            std::cout << "Entity " << entity.getId() << " exploded." << std::endl;
        }
    }
}

int main()
{
    std::cout << "Hello" << std::endl;

    #ifndef NDEBUG
    if(test()){std::cout << "Worked" << std::endl; }
    #endif

    benchmark();

    example();

    return 0;
}
