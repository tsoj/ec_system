ifndef COMP
	COMP = clang++
endif

CFLAGS = -std=c++17 -O3 -Wall -Wextra -Wpedantic -D MAX_NUM_COMPONENT_TYPES=32

ifeq ($(RELEASE),yes)
	CFLAGS += -DNDEBUG
else
	CFLAGS += -fno-omit-frame-pointer -g
endif

ifeq ($(SANITIZE),yes)
	CFLAGS += -fsanitize=undefined -fsanitize=address -fsanitize=leak
else
ifeq ($(SANITIZE_MEMORY),yes)
	CFLAGS += -fsanitize=memory
else
ifeq ($(SANITIZE_THREAD),yes)
	CFLAGS += -fsanitize=thread
endif
endif
endif

LFLAGS = $(CFLAGS) -pthread
ifeq ($(STATIC),yes)
	LFLAGS += -static
endif


ifndef NAME
	NAME = ec_system
endif

ifndef SRC
	SRC = ./
endif

ifndef BUILD
	BUILD = ./build/
endif

CPP = $(wildcard $(SRC)*.cpp)

OBJ = $(CPP:$(SRC)%.cpp=$(BUILD)%.o)

$(BUILD)$(NAME): $(OBJ)
	$(COMP) -o $(BUILD)$(NAME) $(OBJ) $(LFLAGS)

-include $(OBJ:%.o=%.d)

$(BUILD)%.o: $(SRC)%.cpp
	$(COMP) $(CFLAGS) -c $(SRC)$*.cpp -o $(BUILD)$*.o
	echo -n $(BUILD) > $(BUILD)$*.d
	$(COMP) $(CFLAGS) -MM $(SRC)$*.cpp >> $(BUILD)$*.d

.PHONY : clean
.PHONY : test

test: $(BUILD)$(NAME)
	$(BUILD)$(NAME)

clean:
	rm $(BUILD)*.o $(BUILD)*.d $(BUILD)$(NAME)
